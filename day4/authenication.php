<?php

if (session_status() == PHP_SESSION_NONE) {
  session_start();
}
if(!isset($_SESSION['user'])) {
  echo '<p>Access denied. You must be <a href="login.php">login</a></p>';
  $_SESSION['redirect_url'] = 'index.php';
  exit;
}

?>