<?php
include_once './authenication.php';
include_once './util.php';
?>
<h1>Search</h1>
<form action="<?php echo $_SERVER['PHP_SELF']?>" method='GET'>
  <div class="form-group">
    <label for="searchKey">Search</label>
    <input type="text" class="form-control" id="searchKey" placeholder="Searching" name='searchKey'>
  </div>
  <button type="submit" class="btn btn-primary" name='search'>Search</button>
</form>

<?php
if(isset($_GET['search']) && isset($_GET['searchKey']) && $_GET['searchKey'] !== '') {
  $data = getDataSearch($_GET['searchKey']);
  if(count($data) > 0) {
    displayMedia(getDataSearch($_GET['searchKey']));
  } else {
    echo '<div class="alert alert-danger" role="alert">
            No entry match '.$_GET['searchKey'].'
          </div>';
  }
}

?>