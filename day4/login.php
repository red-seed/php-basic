<?php
session_start();
$_err = null;
if(isset($_POST['submitLogin'])) {
  if(isset($_POST['email']) && isset($_POST['password'])) {
    if($_POST['email'] !== '' && $_POST['email'] === $_POST['password']) {
      echo $_POST['email'];
      
      $_SESSION['user'] = $_POST['email'];
      if(isset($_SESSION['redirect_url'])) {
        header('Location: ' . $_SESSION['redirect_url']);
        exit;
      } else {
        header('Location: ' . 'index.php');
        exit;
        echo 'redirected';
      }
    } else {
      $_err = "Email and password are not equal";
    }
  }
}

?>
<html>
<head>
  <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>

<body>
  <div class="container" style='max-width: 400px'>
    <div class="login-form">
      <div class="main-div">
        <div class="panel">
          <h2>Login</h2>
          <p>Please enter your email and password</p>
          <strong>NOTE: because I don't use database so let enter email and password are the same to test :) </strong>
          <strong>example: email: abc@gmail.com password: abc@gmail.com</strong>
          <?php
            if(isset($_err)) {
              echo '<div class="alert alert-danger" role="alert">
                    '.$_err.'
                  </div>';
            }
          ?>
        </div>
        <form id="Login" method='POST'>
          <div class="form-group">
            <input type="text" class="form-control" id="inputEmail" placeholder="Email Address" name="email" value="abc@gmail.com">
          </div>
          <div class="form-group">
            <input type="password" class="form-control" id="inputPassword" placeholder="Password" name="password" value="abc@gmail.com">
          </div>
          <button type="submit" class="btn btn-primary" name="submitLogin">Login</button>
        </form>
       </div>
      </div>
    </div>
  </div>
</body>

</html>