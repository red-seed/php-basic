<h1>Upload Media</h1>
<?php
  function errorFileUpload($errno, $errstr, $errfile, $errline) {
    // echo "<b>error:</b> [$errno] $errstr<br>";
    // echo " Error on line $errline in $errfile<br>";
    throw new \Exception($errfile);
  }
  function uploadFile() {
    if (isset($_FILES['file'])) {
      $file = $_FILES['file'];
      try {
        if ($file['error'] > 0) {
          trigger_error('Faildeeee');
        } else {
            if (!file_exists('./files/')) {
                mkdir('./files/', 0777, true);
            }
            $savingFileName = htmlspecialchars('./files/'
                              .uniqid().'---'
                              .date('d-M-Y').'---'
                              .$_SESSION['user'].'---'
                              .str_replace('/','-',$file['type']).'---'
                              .$file['name']);
            move_uploaded_file($file['tmp_name'], $savingFileName);
            $myfile = file_put_contents('file.txt', $savingFileName.PHP_EOL, FILE_APPEND | LOCK_EX);
            echo '<div class="alert alert-success" role="alert">
              Upload file is success!
            </div>';
        }
      } catch (\Throwable $th) {
        echo '<div class="alert alert-danger" role="alert">
                Upload file is error! The size of file is greater than '.ini_get('upload_max_filesize').'
              </div>';
      }
    }
  }
  set_error_handler("errorFileUpload");

  include_once './authenication.php';

  if (isset($_POST['upload'])) {
    uploadFile();
  }
?>

<form method="POST" action="index.php" enctype="multipart/form-data">
	<div class="form-group">
		<div class="input-group input-file" name="file">
			<span class="input-group-btn">
        	<button class="btn btn-default btn-choose" type="button">Choose</button>
    	</span>
      <input type="text" class="form-control" placeholder='Choose a file...' />
      <span class="input-group-btn">
            <button class="btn btn-warning btn-reset" type="button">Reset</button>
      </span>
      
		</div>
	</div>

	<div class="form-group">
		<button type="submit" class="btn btn-primary pull-right" name="upload">Submit</button>
	</div>
    
</form>