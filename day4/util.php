<?php

function getAll() {
  if(!file_exists('./file.txt')) {
    return array();
  }
  $myfile = fopen('file.txt', "r");
  $dataRow = array();
  while(!feof($myfile)) {
    $row = fgets($myfile);
    if(count(explode('---', $row)) === 5) {
      array_push($dataRow, $row);
    }
  }
  fclose($myfile);
  return $dataRow;
}

function getLimit($limit) {
  if(!file_exists('./file.txt')) {
    return array();
  }
  $data = getAll();
  if(count($data) <= $limit) {
    return $data;
  }

  return array_slice($data, 0, $limit);
}


function getDataSearch($searchKey) {
  if(!file_exists('./file.txt')) {
    return array();
  }
  $myfile = fopen('file.txt', "r");
  $dataRow = array();
  while(!feof($myfile)) {
    $line = fgets($myfile);
    if(stripos($line, $searchKey) !== false) {
      array_push($dataRow, $line);
    }
  }
  fclose($myfile);
  return $dataRow;
}

function displayMedia($data) {
  echo '<div class="row">';
  foreach ($data as $key => $value) {
    $info = explode('---', $value);

    $embed = '';
    if(strpos($info[3], 'video') !== false) {
      $embed = '<video controls>
                  <source src="'.$value.'" type="'.str_replace('-', '/', $info[3]).'">
                </video>';
    } else {
      $embed = '<embed src="'.$value.'"/>';
    }
    echo '<div class="col-3">
            <div class="media-show">
              <h6>'.$info[4].'</h6>
              <p>Owner: '.$info[2].'</p>
              <p>Uploaded Date: '.$info[1].'</p>
              '.$embed.'
            </div>
          </div>';
  }
  echo '</div>';
}

?>