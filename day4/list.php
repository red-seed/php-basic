<h1>List first 10 media</h1>
<?php
if(session_status() == PHP_SESSION_NONE) {
  session_start();
}

?>
<form action="<?php echo $_SERVER['PHP_SELF']?>" method='POST'>
  <?php 
  
    if(isset($_POST['show'])) {
      $_SESSION['show'] = true;
    } else if(isset($_POST['hide'])) {
      $_SESSION['show'] = false;
    } 
    
    if(isset($_SESSION['show'])) {
      if($_SESSION['show'] == true) {
        echo '<button type="submit" class="btn btn-primary" name="hide">HIDE</button>';
      } else {
        echo '<button type="submit" class="btn btn-primary" name="show">SHOW</button>';
      }
    }
    else {
      echo '<button type="submit" class="btn btn-primary" name="show">SHOW</button>';
    }
  
  ?>
  
</form>

<?php


if(isset($_SESSION['show']) && $_SESSION['show']) {
  
  include_once './authenication.php';
  include_once './util.php';
  displayMedia(getLimit(10));
}


?>

