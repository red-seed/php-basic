<?php
session_start();

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <title>Upload media</title>

  <style>
    .media-show {
      text-align: center;
      border: 1px solid black;
      margin-top: 10px;
    }

    .media-show embed, video {
      width: 100%;
      height: 100%;
    }
  </style>
</head>
<body>

<div class="container">

<?php
  include_once './menu.php';
  $_SESSION['redirect_url'] = 'index.php';

  if(isset($_SESSION['user'])) {
    include_once './upload.php';
    include_once './search.php';
    include_once './list.php';
  } else {
    echo '<h6>User must be login to access functions</h6>';
  }
?>

</div> 


<script src="https://code.jquery.com/jquery-3.1.1.min.js">
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src='search.js'></script>  
</body>
</html>