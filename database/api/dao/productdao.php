<?php 
include_once '../config/database.php';

class ProductDAO {
  private $database;
  private $table_name = "products";

  public function __construct() {
    $this->database = new Database();
  }

  function getAllProducts() {
    $con = $this->database->getConnection();
    $query = "SELECT
      c.name as category_name, p.id, p.name, p.description, p.price, p.category_id, p.created
    FROM
      " . $this->table_name . " p
      LEFT JOIN
          categories c
              ON p.category_id = c.id
    ORDER BY
      p.created DESC"; 
    
    $stmt = $con->prepare($query);
    $stmt->execute();
    $num  = $stmt->rowCount();

    if($num > 0) {
      $products_arr             = array();
      $products_arr["length"]   = 0;
      $products_arr["products"] = array();

      // retrieve our table contents
      // fetch() is faster than fetchAll()
      // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
      while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        extract($row);
        // print_r($row);

        $product_item=array(
          "id" => $id,
          "name" => $name,
          "description" => html_entity_decode($description),
          "price" => $price,
          "category_id" => $category_id,
          "category_name" => $category_name
        );

        array_push($products_arr["products"], $product_item);
      }

      $products_arr["length"] = count($products_arr["products"]);
      $con = null;
      return $products_arr;
    } else {
      return array();
    }
  }
}

?>