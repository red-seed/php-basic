<?php
    use App\Model\PersonModel;
    use Cu\Db\Migration;

    $ps = new PersonModel();

    $result =$ps->all();
    $people = null;

    if(!isset($result['err'])) {
      $people = $result['data'];
    } else {
      if (!Migration::migrate()) {
          echo "Failed to initialize database";
      } else {
          $result =$ps->all();
          $people = $result['data'];
      }
    }
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Bootstrap Table with Add and Delete Row Feature</title>
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round|Open+Sans">
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="styles.css">
 
</head>

<body>
  <div class="container">
    
    <div class="table-wrapper">
      
    <div class="alert alert-success" role="alert" id="sucess">
      This is a success alert—check it out!
    </div>

    <div class="alert alert-danger" role="alert" id="fail">
      This is a danger alert—check it out!
    </div>

      <?php
          if($people == null) { ?>
            <form action="migrate" method='POST'>
              <div class="form-group " style="text-align:center">
                <label for="searchKey">Data is empty, you must initialize it</label><div></div>
                <button type="submit" class="btn btn-primary" name='init_data'>Initialize data</button>
              </div>
            </form>
      <?php
          } else { ?>
              <form action="migrate" method='POST'>
                <div class="form-group " >
                  <button type="submit" class="btn btn-primary" name='init_data'>Reset Data</button>
                </div>
              </form>

              <div class="table-title">
                <div class="row">
                  <div class="col-sm-8">
                    <h2>Employee <b>Details</b></h2>
                  </div>
                  <div class="col-sm-4">
                    <button type="button" class="btn btn-info add-new"><i class="fa fa-plus"></i> Add New</button>
                  </div>
                </div>
              </div>
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                    foreach ($people as $key => $person) { 
                      if ($person['is_active'] != 0) {
                          ?>
                      <tr data-id="<?php echo $person['id']?>">
                        <td col-name="first_name"><?php echo $person['first_name']?></td>
                        <td col-name="last_name"><?php echo $person['last_name']?></td>
                        <td col-name="email"><?php echo $person['email']?></td>
                        <td>
                          <a class="add" title="Add" data-toggle="tooltip"><i class="material-icons">&#xE03B;</i></a>
                          <a class="edit" title="Edit" data-toggle="tooltip"><i class="material-icons">&#xE254;</i></a>
                          <a class="delete" title="Delete" data-toggle="tooltip"><i class="material-icons">&#xE872;</i></a>
                        </td>
                      </tr>
                  <?php
                      }}
                  ?>
                </tbody>
              </table>
      <?php }
      ?>
    </div>
  </div>

  <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="main.js"></script>
</body>

</html>                     