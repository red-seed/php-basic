<?php

use App\Model\PersonModel;

$route->get('/api/user', 'ApiPersonController@getAll');

$route->post('/api/user', 'ApiPersonController@addPerson');

$route->put('/api/user/{id}','ApiPersonController@updatePerson');

$route->delete('/api/user/{id}', 'ApiPersonController@deltePerson');
