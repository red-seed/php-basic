<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <title>Document</title>
</head>
<body>

  
  <?php
    define("FILE_NAME", "data.txt");
    if(isset($_POST['add'])) {
      if(isset($_POST['name']) && isset($_POST['yob'])) {
        $name = $_POST['name'];
        $yob = $_POST['yob'];
        $lan = $_POST['lan'];
        $lineSave = uniqid().'|'.$name.'|'.$yob.'|'.$lan;
        $myfile = file_put_contents(FILE_NAME, $lineSave.PHP_EOL , FILE_APPEND | LOCK_EX);
      }
    }
  ?>

  <?php
    try {
      if (file_exists(FILE_NAME) ) {
      $myfile = fopen(FILE_NAME, "r") or die("Unable to open file!");
      $dataRaw = array();
      while(!feof($myfile)) {
        $line = explode('|', fgets($myfile));
        if(count($line) === 4) {
          array_push(
            $dataRaw,
            array (
            'id' => $line[0],
            'name' => $line[1],
            'yob' => $line[2],
            'lan' => $line[3]
            )
          );
        }
      }
      fclose($myfile);
      $data = array();
      if(isset($_POST['view'])) {
        $data = $dataRaw;
      }

    }

      } catch (Exception $e) {
        echo 'Caught exception: ',  $e->getMessage(), "\n";
    }
    ?>

  <div class="container">
    <h1>Post data from a Form input then save data to file  </h1>
    <form action="<?php echo $_SERVER['PHP_SELF']?>" method='POST'>
      <div class="form-group">
        <label for="name">Name</label>
        <input type="text" class="form-control" id="name" placeholder="Full Name" name='name'>
      </div>
      <div class="form-group">
        <label for="yob">First Argument</label>
        <input type="number" class="form-control" id="yob" placeholder="Year of birth" name='yob' >
      </div>
      <div class="form-group">
        <label for="lan">Language Programming</label>
        <input type="text" class="form-control" id="lan" placeholder="Language Programming" name='lan'>
      </div>
      <button type="submit" class="btn btn-primary" name='add'>Save</button>
    </form>

    <h1 style='margin-top: 50px'>Get all data in file</h1>
    <form action="<?php echo $_SERVER['PHP_SELF']?>" method='POST' >
      <button type="submit" class="btn btn-primary" name='view'>View All</button>
    </form>
      
    <h1 style='margin-top: 50px'><h1 style='margin-top: 50px'>Search data by name.</h1></h1>
     <form action="<?php echo $_SERVER['PHP_SELF']?>" method='POST'>
      <div class="form-group">
        <label for="searchKey">Search by name</label>
        <input type="text" class="form-control" id="searchKey" placeholder="searching..." name='searchKey'>
      </div>
      <button type="submit" class="btn btn-primary" name='search'>Search</button>
    </form>
    <?php
      function _readFile() {
        $myfile = fopen(FILE_NAME, "r") or die("Unable to open file!");
        $dataRow = array();
        $searchedRow = null;
        while(!feof($myfile)) {
          $line = explode('|', fgets($myfile));
          if(count($line) === 4) {
            $searchedRow = array (
              'id' => $line[0],
              'name' => $line[1],
              'yob' => $line[2],
              'lan' => $line[3]
            );
            array_push(
              $dataRow,
              $searchedRow
            );
            break;
          }
        }
        fclose($myfile);
        return $dataRow;
      }
    ?>

    <?php
      if(isset($_POST['viewById'])) {
        $myfile = fopen(FILE_NAME, "r") or die("Unable to open file!");
        $data = array();
        $searchedRow = null;
        while(!feof($myfile)) {
          $line = explode('|', fgets($myfile));
          if(count($line) === 4 && $line[0] == $_POST['idRecourDetail']) {
            $searchedRow = array (
              'id' => $line[0],
              'name' => $line[1],
              'yob' => $line[2],
              'lan' => $line[3]
            );
            array_push(
              $data,
              $searchedRow
            );

            break;
          }
        }
        fclose($myfile);
      }
    ?>
    
    <?php
      if(isset($dataRaw) && count($dataRaw) > 0) {
        $lines = '';

        foreach($dataRaw as $value) {
          $lines .= '<a class="dropdown-item" data="'.$value['id'].'">'.$value['name'].'</a>';
        }
        $dataFirstView = $dataRaw[0];
        if(isset($searchedRow)) {
          $dataFirstView = $searchedRow;
        }
        
        $view = '<h1 style="margin-top: 50px">Search by Id</h1>
                  <form method="POST">
                  <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data="'.$dataFirstView['id'].'" name="adf">
                      '.$dataFirstView['name'].'
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                      '.$lines.'
                    </div>
                  </div>
                  <input type="hidden" id="id-hidden" name="idRecourDetail" value="'.$dataFirstView['id'].'">
                  <button type="submit" class="btn btn-primary" name="viewById">View</button>
                </form>';

        echo $view;
      } else {
        echo "<p>Data is emtpy</p>";
      }
    ?>

   
    
    <?php
      if(isset($_POST['search']) && isset($_POST['searchKey']) && $_POST['searchKey'] !== '') {
        $searchKey = $_POST['searchKey'];
        $myfile = fopen(FILE_NAME, "r") or die("Unable to open file!");
        $data = array();
        while(!feof($myfile)) {
          $line = explode('|', fgets($myfile));
          if(count($line) === 4 && strpos($line[1], $searchKey) !== false) {
            array_push(
              $data,
              array (
              'id' => $line[0],
              'name' => $line[1],
              'yob' => $line[2],
              'lan' => $line[3]
              )
            );
          }
        }
        fclose($myfile);
      }
    ?>


    <?php
      if(isset($data) && count($data) > 0) {
        $row = '';
        // print_r($data);
        foreach ($data as $key => $value) {
        $row .= '<tr>
                    <th scope="row">'. $value['id']. '</th>
                    <td>'. $value['name']. '</td>
                    <td>'. $value['yob']. '</td>
                    <td>'. $value['lan']. '</td>
                  </tr>';
        }
        echo '<table class="table" style="margin-top: 50px">
                <thead>
                  <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Name</th>
                    <th scope="col">Year of Birth</th>
                    <th scope="col">Language Programming</th>
                  </tr>
                </thead>
                <tbody>
                  '.$row.'
              </table>';
      }
    ?>

  </div>

  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script>
      var lastType = '#';
      $(".dropdown-menu a").click(function(){
        var newType = $(this).attr('data');
        
        if(newType != lastType) {
            lastType = newType;
            var btn = $(this).parent().prev();
            btn.text($(this).text());
            btn.attr('data', newType);
            $('#id-hidden').attr('value', newType);
            var value = $('.search__input').val().trim();
            if(value.length > 0) {
                getRecommandSearch(newType, value);
            }
        }
       
   });

</script>
</body>
</html>

